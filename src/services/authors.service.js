import client from "./client";

const table = client.getTable("author");

class AuthorsService {
  async getAuthors() {
    try {
      const response = await table.read();
      // console.log({ response });
      if (response.data) {
        return response.data;
      }
    } catch (error) {
      // console.error(error);
      if (error.response.status === 401) {
        window.location = "/";
      }
      return { error };
    }
  }

  async addAuthor({ name }) {
    try {
      const response = await table.insert({
        name,
      });
      // console.log({ response });
      if (response) {
        return response;
      }
    } catch (error) {
      // console.error(error);
      if (error.response.status === 401) {
        window.location = "/";
      }
      return { error };
    }
  }

  async updateAuthor({ id, name }) {
    try {
      const response = await table.update({
        id,
        data: {
          name,
        },
      });
      // console.log({ response });
      if (response) {
        return response;
      }
    } catch (error) {
      // console.error(error);
      if (error.response.status === 401) {
        window.location = "/";
      }
      return { error };
    }
  }

  async deleteAuthor({ id }) {
    try {
      const response = await table.del({ id });
      // console.log({ response });
      if (response) {
        return response;
      }
    } catch (error) {
      // console.error(error);
      if (error.response.status === 401) {
        window.location = "/";
      }
      return { error };
    }
  }
}

export default new AuthorsService();

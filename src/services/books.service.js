import client from "./client";

const table = client.getTable("book");
const fill = table.composeFilter;

class BooksService {
  async getBooks({ id }) {
    try {
      const response = await table
        .filter(fill().eq("author_id", id).toOData())
        .read();
      // console.log(result);
      if (response.data) {
        return response.data;
      }
    } catch (error) {
      console.error(error);
      // if (error.response.status === 401) {
      //   window.location = "/";
      // }
      return { error };
    }
  }

  async addBook({ title, total_pages, author_id }) {
    try {
      const response = await table.insert({
        title,
        total_pages,
        author_id,
      });
      // console.log({ response });
      if (response) {
        return response;
      }
    } catch (error) {
      // console.error(error);
      if (error.response.status === 401) {
        window.location = "/";
      }
      return { error };
    }
  }

  async updateBook({ id, title, total_pages }) {
    try {
      const response = await table.update({
        id,
        data: {
          title,
          total_pages,
        },
      });
      // console.log({ response });
      if (response) {
        return response;
      }
    } catch (error) {
      // console.error(error);
      if (error.response.status === 401) {
        window.location = "/";
      }
      return { error };
    }
  }

  async deleteBook({ id }) {
    try {
      const response = await table.del({ id });
      // console.log({ response });
      if (response) {
        return response;
      }
    } catch (error) {
      // console.error(error);
      if (error.response.status === 401) {
        window.location = "/";
      }
      return { error };
    }
  }
}

export default new BooksService();

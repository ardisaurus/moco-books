import client from "./client";

class AuthService {
  async login({ email, password }) {
    try {
      const loginRes = await client.auth.login("local", {
        email,
        password,
      });
      return loginRes;
    } catch (error) {
      // console.log({ error });
      return { error };
    }
  }

  async logout() {
    try {
      await client.user.logout();
      return true;
    } catch (error) {
      console.log({ error });
      return { error };
    }
  }

  getLoginStatus() {
    const exp = Math.round(
      new Date(localStorage.getItem("MBaaS.auth.expiry")).getTime() / 1000
    );
    const now = Math.round(new Date() / 1000);
    if (now > exp) {
      return false;
    }
    return true;
  }
}

export default new AuthService();

import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import EditIcon from "@material-ui/icons/Edit";
import ConfirmRemove from "../dialogs/ConfirmRemove";
import BookForm from "../dialogs/BookForm";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  tableHeader: {
    backgroundColor: "#c5cbe9",
  },
});

export default function BasicTable({ books = [], handleUpdate, handleRemove }) {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead className={classes.tableHeader}>
          <TableRow>
            <TableCell>Title</TableCell>
            <TableCell>Total Pages</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {books.map((book) => (
            <TableRow key={book.id}>
              <TableCell component="th" scope="row">
                {book.title}
              </TableCell>
              <TableCell>{book.total_pages}</TableCell>
              <TableCell style={{ width: "280px" }}>
                <Grid container justifyContent="flex-end" spacing={1}>
                  <Grid item>
                    <BookForm
                      label="Edit"
                      buttonIcon={<EditIcon />}
                      titleValue={book.title}
                      totalPagesValue={book.total_pages}
                      handleSubmit={({ title, total_pages }) => {
                        handleUpdate({ id: book.id, title, total_pages });
                      }}
                    />
                  </Grid>
                  <Grid item>
                    <ConfirmRemove
                      handleConfirm={() => {
                        handleRemove({ id: book.id });
                      }}
                    />
                  </Grid>
                </Grid>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

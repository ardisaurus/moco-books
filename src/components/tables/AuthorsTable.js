import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import BookIcon from "@material-ui/icons/Book";
import EditIcon from "@material-ui/icons/Edit";
import AuthorForm from "../dialogs/AuthorForm";
import ConfirmRemove from "../dialogs/ConfirmRemove";
import { useNavigate } from "react-router-dom";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  tableHeader: {
    backgroundColor: "#c5cbe9",
  },
});

export default function BasicTable({
  authors = [],
  handleRemove,
  handleUpdate,
}) {
  const navigate = useNavigate();
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead className={classes.tableHeader}>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {authors.map((author) => (
            <TableRow key={author.id}>
              <TableCell component="th" scope="row">
                {author.name}
              </TableCell>
              <TableCell style={{ width: "280px" }}>
                <Grid container justifyContent="flex-end" spacing={1}>
                  <Grid item>
                    <Button
                      variant="contained"
                      size="small"
                      onClick={() => {
                        navigate(`/books/${author.id}`);
                      }}
                      startIcon={<BookIcon />}
                    >
                      Books
                    </Button>
                  </Grid>
                  <Grid item>
                    <AuthorForm
                      label="Edit"
                      nameValue={author.name}
                      buttonIcon={<EditIcon />}
                      handleSubmit={({ name }) => {
                        handleUpdate({ id: author.id, name });
                      }}
                    />
                  </Grid>
                  <Grid item>
                    <ConfirmRemove
                      handleConfirm={() => {
                        handleRemove({ id: author.id });
                      }}
                    />
                  </Grid>
                </Grid>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

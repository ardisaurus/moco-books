import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useFormik } from "formik";
import * as Yup from "yup";

const validationSchema = Yup.object().shape({
  title: Yup.string().min(3).required("Required"),
  totalPages: Yup.number().min(1).required("Required"),
});

export default function FormDialog({
  label,
  buttonIcon,
  titleValue = "",
  totalPagesValue = 0,
  handleSubmit,
}) {
  const [open, setOpen] = React.useState(false);

  const formik = useFormik({
    initialValues: {
      title: "",
      totalPages: 0,
    },
    validationSchema: validationSchema,
    validateOnBlur: false,
    validateOnChange: false,
    onSubmit: async ({ title, totalPages }) => {
      handleSubmit({ title, total_pages: totalPages });
      handleClose();
    },
  });

  const handleClickOpen = () => {
    formik.setFieldValue("title", titleValue);
    formik.setFieldValue("totalPages", totalPagesValue);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="contained"
        color="primary"
        startIcon={buttonIcon}
        size="small"
        onClick={handleClickOpen}
      >
        {label}
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        maxWidth="sm"
        fullWidth
      >
        <DialogTitle id="form-dialog-title">{label} Book</DialogTitle>
        <form onSubmit={formik.handleSubmit}>
          <DialogContent dividers>
            <TextField
              autoFocus
              type="text"
              margin="dense"
              id="title"
              label="Title"
              variant="outlined"
              name="title"
              value={formik.values.title}
              onChange={formik.handleChange}
              error={Boolean(formik.errors.title) && formik.touched.title}
              helperText={formik.errors.title}
              fullWidth
            />
            <TextField
              type="number"
              margin="dense"
              id="totalPages"
              label="Total Pages"
              variant="outlined"
              name="totalPages"
              value={formik.values.totalPages}
              onChange={formik.handleChange}
              error={
                Boolean(formik.errors.totalPages) && formik.touched.totalPages
              }
              helperText={formik.errors.totalPages}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button variant="outlined" onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button variant="contained" type="submit" color="primary">
              Submit
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
}

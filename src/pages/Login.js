import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Alert from "@material-ui/lab/Alert";

import { useFormik } from "formik";
import React, { useState } from "react";
import * as Yup from "yup";
import authServices from "../services/auth.service";
import { useNavigate } from "react-router-dom";

const validationSchema = Yup.object().shape({
  email: Yup.string().email("Invalid Email.").required("Required"),
  password: Yup.string()
    .min(5, "Use combination of 6 character or more")
    .required("Insert password"),
});

function Login() {
  const navigate = useNavigate();
  const [errorMsg, setErrorMsg] = useState("");
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: validationSchema,
    validateOnBlur: false,
    validateOnChange: false,
    onSubmit: async ({ email, password }) => {
      // console.log({ email, password });
      const result = await authServices.login({
        email,
        password,
      });
      if (!Boolean(result.error)) {
        navigate("/home");
      } else {
        setErrorMsg(result.error.title);
      }
    },
  });

  return (
    <Grid
      container
      justifyContent="center"
      alignItems="center"
      style={{ height: "100vh", backgroundColor: "lightgray" }}
    >
      <Paper style={{ padding: "1em", minWidth: "20em" }}>
        <Grid container direction="column" spacing={1}>
          <Grid item>
            <Typography variant="h4">Books App</Typography>
          </Grid>
          {Boolean(errorMsg) && (
            <Grid item>
              <Alert severity="error">{errorMsg}</Alert>
            </Grid>
          )}
          <form onSubmit={formik.handleSubmit}>
            <Grid item>
              <TextField
                type="text"
                variant="outlined"
                margin="dense"
                label="email"
                name="email"
                value={formik.values.email}
                onChange={formik.handleChange}
                error={Boolean(formik.errors.email) && formik.touched.email}
                helperText={formik.errors.email}
                fullWidth
              ></TextField>
            </Grid>
            <Grid item>
              <TextField
                type="password"
                variant="outlined"
                margin="dense"
                label="Password"
                name="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                error={
                  Boolean(formik.errors.password) && formik.touched.password
                }
                helperText={formik.errors.password}
                fullWidth
              ></TextField>
            </Grid>
            <Grid item style={{ padding: "1em 0.4em" }}>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
              >
                Log in
              </Button>
            </Grid>
          </form>
        </Grid>
      </Paper>
    </Grid>
  );
}

export default Login;

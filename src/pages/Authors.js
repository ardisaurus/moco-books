import Appbar from "../components/Appbar";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Alert from "@material-ui/lab/Alert";
import AuthorsTable from "../components/tables/AuthorsTable";
import AuthorForm from "../components/dialogs/AuthorForm";
import AddIcon from "@material-ui/icons/Add";
import { Fragment } from "react";
import React, { useState, useEffect } from "react";
import AuthorsService from "../services/authors.service";

function Authors() {
  const [authors, setAuthors] = useState([]);
  const [errorMsg, setErrorMsg] = useState("");
  const [successMsg, setSuccessMsg] = useState("");
  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const result = await AuthorsService.getAuthors();
    if (!Boolean(result.error)) {
      setAuthors(result);
    } else {
      setErrorMsg(result.error.title);
    }
  };

  const addAuthor = async ({ name }) => {
    const result = await AuthorsService.addAuthor({ name });
    if (!Boolean(result.error)) {
      getData();
      setSuccessMsg("Data has been added.");
    } else {
      setErrorMsg(result.error.title);
    }
  };

  const updateAuthor = async ({ id, name }) => {
    const result = await AuthorsService.updateAuthor({ id, name });
    if (!Boolean(result.error)) {
      getData();
      setSuccessMsg("Data has been updated.");
    } else {
      setErrorMsg(result.error.title);
    }
  };

  const removeAuthor = async ({ id }) => {
    const result = await AuthorsService.deleteAuthor({ id });
    if (!Boolean(result.error)) {
      getData();
      setSuccessMsg("Data has been removed.");
    } else {
      setErrorMsg(result.error.title);
    }
  };

  return (
    <Fragment>
      <Appbar />
      <Container maxWidth="lg">
        {Boolean(errorMsg) && <Alert severity="error">{errorMsg}</Alert>}
        {Boolean(successMsg) && <Alert severity="success">{successMsg}</Alert>}
        <Grid
          container
          justifyContent="space-between"
          alignItems="center"
          style={{ padding: "0.5em 0" }}
        >
          <Grid item>
            <Typography variant="h4">Authors</Typography>
          </Grid>
          <Grid item>
            <AuthorForm
              label="Add"
              buttonIcon={<AddIcon />}
              handleSubmit={addAuthor}
            />
          </Grid>
        </Grid>
        <AuthorsTable
          authors={authors}
          handleRemove={removeAuthor}
          handleUpdate={updateAuthor}
        />
      </Container>
    </Fragment>
  );
}

export default Authors;

import Appbar from "../components/Appbar";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import BackIcon from "@material-ui/icons/ArrowBack";
import AddIcon from "@material-ui/icons/Add";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Alert from "@material-ui/lab/Alert";
import BookTable from "../components/tables/BookTable";
import BookForm from "../components/dialogs/BookForm";
import { Fragment, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import BooksService from "..//services/books.service";

function Books() {
  const navigate = useNavigate();
  let { id } = useParams();
  const [books, setBooks] = useState([]);
  const [errorMsg, setErrorMsg] = useState("");
  const [successMsg, setSuccessMsg] = useState("");
  useEffect(() => {
    getData();
    // eslint-disable-next-line
  }, []);

  const getData = async () => {
    const result = await BooksService.getBooks({ id });
    // console.log({ result });
    if (!Boolean(result.error)) {
      setBooks(result);
    } else {
      setBooks(result.error.title);
    }
  };

  const addBook = async ({ title, total_pages }) => {
    const result = await BooksService.addBook({
      title,
      total_pages,
      author_id: id,
    });
    if (!Boolean(result.error)) {
      getData();
      setSuccessMsg("Data has been added.");
    } else {
      setErrorMsg(result.error.title);
    }
  };

  const updateBook = async ({ id, title, total_pages }) => {
    const result = await BooksService.updateBook({ id, title, total_pages });
    if (!Boolean(result.error)) {
      getData();
      setSuccessMsg("Data has been updated.");
    } else {
      setErrorMsg(result.error.title);
    }
  };

  const removeBook = async ({ id }) => {
    const result = await BooksService.deleteBook({ id });
    if (!Boolean(result.error)) {
      getData();
      setSuccessMsg("Data has been removed.");
    } else {
      setErrorMsg(result.error.title);
    }
  };

  return (
    <Fragment>
      <Appbar />
      <Container maxWidth="lg">
        {Boolean(errorMsg) && <Alert severity="error">{errorMsg}</Alert>}
        {Boolean(successMsg) && <Alert severity="success">{successMsg}</Alert>}
        <Grid
          container
          justifyContent="space-between"
          alignItems="center"
          style={{ padding: "0.5em 0" }}
        >
          <Grid item>
            <Grid container>
              <Button variant="text" onClick={() => navigate("/home")}>
                <BackIcon />
              </Button>
              <Typography variant="h4">Books</Typography>
            </Grid>
          </Grid>
          <Grid item>
            <BookForm
              label="Add"
              buttonIcon={<AddIcon />}
              handleSubmit={addBook}
            />
          </Grid>
        </Grid>
        <BookTable
          books={books}
          handleRemove={removeBook}
          handleUpdate={updateBook}
        />
      </Container>
    </Fragment>
  );
}

export default Books;

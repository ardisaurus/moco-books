import routes from "./routes";
import { BrowserRouter as Router, useRoutes } from "react-router-dom";
import AuthService from "./services/auth.service";

function App() {
  const isLoggedIn = AuthService.getLoginStatus();
  const routing = useRoutes(routes(isLoggedIn));
  return <>{routing}</>;
}

const AppWrapper = () => {
  return (
    <Router>
      <App />
    </Router>
  );
};

export default AppWrapper;

import { Navigate } from "react-router-dom";
import Login from "./pages/Login";
import Authors from "./pages/Authors";
import Books from "./pages/Books";

const routes = (isLoggedIn) => [
  {
    // protected routes
    path: "/home",
    element: isLoggedIn ? <Authors /> : <Navigate to="/" />,
  },
  {
    // protected routes
    path: "/books/:id",
    element: isLoggedIn ? <Books /> : <Navigate to="/" />,
  },
  {
    // public routes
    path: "/",
    element: !isLoggedIn ? <Login /> : <Navigate to="/home" />,
  },
];

export default routes;
